const gulp = require('gulp'),
	watch = require('gulp-watch'),

	sass = require('gulp-sass'),
	uglify = require('gulp-uglify'),
	htmlmin = require('gulp-htmlmin'),
	cleanCSS = require('gulp-clean-css'),
	csscomb = require('gulp-csscomb'),
	autoprefixer = require('gulp-autoprefixer'),

	sourcemaps = require('gulp-sourcemaps'),
	cache = require('gulp-cache'),
	rigger = require('gulp-rigger'),
	rimraf = require('rimraf'),
	rename = require('gulp-rename'),
	notify = require('gulp-notify'),
	size = require('gulp-size'),
	zip = require('gulp-zip'),

	imagemin = require('gulp-imagemin'),
	imageminPngquant = require('imagemin-pngquant'),
	imageminZopfli = require('imagemin-zopfli'),
	imageminMozjpeg = require('imagemin-mozjpeg'),
	imageminGiflossy = require('imagemin-giflossy'),
	webp = require('gulp-webp'),

	ftp = require('vinyl-ftp');

const path = {
	build: {
		php: 'build/',
		html: 'build/',
		js: 'build/js/',
		css: 'build/css/',
		img: 'build/img/',
		fonts: 'build/fonts/',
		maps: 'build/maps/',
		dir: 'build/*'
	},
	src: {
		html: 'src/*.html',
		php: 'src/*.php',
		js: 'src/js/scripts.js',
		style: 'src/scss/style.scss',
		img: 'src/img/**/*.*',
		fonts: 'src/fonts/**/*.*'
	},
	watch: {
		php: 'src/*.php',
		html: 'src/*.html',
		js: 'src/js/**/*.js',
		style: 'src/scss/**/*.scss',
		img: 'src/img/**/*.*',
		fonts: 'src/fonts/**/*.*'
	},
	clean: './build/**/*.*'
};


gulp.task('html:build', gulp.series(function () {
	return gulp.src(path.src.html)
		.pipe(rigger())
		.pipe(htmlmin({
			collapseWhitespace: true,
			useShortDoctype: true,
			minifyCSS: true,
			minifyJS: true
		}))
		.pipe(size({
			title: ' ',
			showFiles: true
		}))
		.pipe(gulp.dest(path.build.html))
	//.pipe(reload({stream: true}));
}));

gulp.task('php:build', function () {
	return gulp.src(path.src.php)
		.pipe(rigger())
		.pipe(htmlmin({
			collapseWhitespace: true,
			useShortDoctype: true,
			minifyCSS: true,
			minifyJS: true
		}))
		.pipe(size({
			title: ' ',
			showFiles: true
		}))
		.pipe(gulp.dest(path.build.html))
	//.pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
	return gulp.src(path.src.js)
	//.pipe(cache('js'))
		.pipe(rigger())
		.pipe(sourcemaps.init())
		.pipe(uglify())
		.pipe(sourcemaps.write('/'))
		.pipe(size({
			title: ' ',
			showFiles: true
		}))
		.pipe(gulp.dest(path.build.js))
	//.pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
	return gulp.src(path.src.style)
	//.pipe(cache('css'))
		.pipe(sourcemaps.init())
		.pipe(
			sass().on('error', notify.onError({
				title: 'Sass Error',
				subtitle: [
					'<%= error.relativePath %>',
					'<%= error.line %>'
				].join(':'),
				message: '<%= error.messageOriginal %>',
				open: 'file://<%= error.file %>',
				onLast: true
			}))
		)
		// .pipe(
		// 	sass().on('error', ()=>{
		// 		console.log();
		// 	} )
		// )
		.pipe(autoprefixer(['>1%', 'last 4 versions', 'Firefox ESR', 'not ie < 10']))
		.pipe(csscomb())
		.pipe(cleanCSS())
		.pipe(rename({
			suffix: '.min',
			prefix: ''
		}))
		.pipe(sourcemaps.write('/'))
		.pipe(size({
			title: ' ',
			showFiles: true
		}))
		.pipe(gulp.dest(path.build.css))
	//.pipe(reload({stream: true}));
});

gulp.task('image:build:webp', function () {
	return gulp.src(path.src.img)
		.pipe(webp({
			quality: 80,
			preset: 'photo',
			method: 6
		}))
		.pipe(size({
			title: ' ',
			showFiles: true
		}))
		.pipe(gulp.dest(path.build.img));
});

gulp.task('image:build', gulp.series(function () {
	return gulp.src(path.src.img)
		.pipe(cache(imagemin([
			//png
			imageminPngquant({
				speed: 1,
				quality: 98
			}),
			imageminZopfli({
				more: true
				// iterations: 50 // very slow but more effective
			}),
			//gif
			// imagemin.gifsicle({
			//     interlaced: true,
			//     optimizationLevel: 3
			// }),
			//gif very light lossy, use only one of gifsicle or Giflossy
			imageminGiflossy({
				optimizationLevel: 3,
				optimize: 3,
				lossy: 2
			}),
			//svg
			imagemin.svgo({
				plugins: [{
					removeViewBox: false
				}]
			}),
			//jpg lossless
			imagemin.jpegtran({
				progressive: true
			}),
			//jpg very light lossy, use vs jpegtran
			imageminMozjpeg({
				quality: 85
			})
		], {
			verbose: true,
			progressive: true
		})))
		.pipe(size({
			title: ' ',
			showFiles: true
		}))
		.pipe(gulp.dest(path.build.img)) //И бросим в build

}, "image:build:webp"));

gulp.task('fonts:build', function () {
	return gulp.src(path.src.fonts)
		.pipe(size({
			title: ' ',
			showFiles: true
		}))
		.pipe(gulp.dest(path.build.fonts))
});

gulp.task('dist:zip', function () {
	var now = new Date();
	var d = now.getDate();
	var m = now.getMonth();
	var y = now.getFullYear();
	var h = now.getHours();
	var m = now.getMinutes();
	var s = now.getSeconds();
	var zipName = d + "-" + m + "-" + y + "_" + h + "-" + m + "-" + s;
	return gulp.src(path.build.dir)
		.pipe(zip(zipName + '.zip'))
		.pipe(size({
			title: ' ',
			showFiles: true
		}))
		.pipe(gulp.dest('./'));
});

gulp.task('clean', gulp.series(function () {
	return cache.clearAll();
}, function (cb) {
	return rimraf(path.clean, cb);
}));

gulp.task('build', gulp.series(
	'clean',
	'fonts:build',
	'style:build',
	'js:build',
	'html:build',
	'php:build',
	'image:build',
	function () {
		return gulp.src(
			'src/.htaccess',
			{dot: true}
		)
			.pipe(size({
				title: ' ',
				showFiles: true
			}))
			.pipe(gulp.dest(path.build.html));
	},
	'dist:zip'
));


gulp.task('watch', function () {
	gulp.watch([path.watch.html], gulp.series('html:build'));
	gulp.watch([path.watch.php], gulp.series('php:build'));
	gulp.watch([path.watch.style], gulp.series('style:build'));
	gulp.watch([path.watch.js], gulp.series('js:build'));
	gulp.watch([path.watch.img], gulp.series('image:build'));
	gulp.watch([path.watch.fonts], gulp.series('fonts:build'));
})