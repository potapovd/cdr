//= plugins/jquery-3.3.1.min.js
//= plugins/webfontloader.min.js

function addScript(src) {
	var script = document.createElement('script');
	script.src = src;
	script.type = 'text/javascript';
	script.async = false;
	document.head.appendChild(script);
}



$(document).ready(function () {
	WebFont.load({
		google: {
			families: ['Open Sans']
		},
		custom: {
			families: ['My Font', 'My Other Font:n4,i4,n7'],
			urls: ['/fonts.css']
		}
	});


	window.onload = function () {
		if (window.jQuery) {
			// jQuery is loaded
			alert("Yeah!");
		} else {
			// jQuery is not loaded
			alert("Doesn't Work");
		}
	}

	//addScript("https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU");


});